import matplotlib.pyplot as plt
from skimage import data
from skimage import io
from skimage.color import rgb2gray

Gambar = io.imread("bird.jpg")
# print(Gambar)
gray = rgb2gray(Gambar)
fig, axes = plt.subplots(1,2, figsize=(8,4))
ax = axes.ravel()
ax[0].imshow(Gambar)
ax[0].set_title("Original")
ax[1].imshow(gray, cmap=plt.cm.gray)
ax[1].set_title("Grayscale")
# print(gray)
# plt.plot([1,2,3,4], [1, 5, 13, 21])
# plt.xlabel('x numbers')
# plt.ylabel('y numbers')

fig.tight_layout()
plt.show()
